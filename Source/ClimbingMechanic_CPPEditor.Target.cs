// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class ClimbingMechanic_CPPEditorTarget : TargetRules
{
	public ClimbingMechanic_CPPEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		ExtraModuleNames.Add("ClimbingMechanic_CPP");
	}
}
