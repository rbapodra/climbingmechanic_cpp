// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "MyAnimInstance.generated.h"

/**
 * Creating a custom AnimInstance class handling the animation updates of the 3rd person character
 */
UCLASS()
class CLIMBINGMECHANIC_CPP_API UMyAnimInstance : public UAnimInstance
{
	GENERATED_BODY()
	
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Animation")
	bool bIsInAir;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animation")
	float Speed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animation")
	bool bIsHanging;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animation")
		bool bCanMoveLeft;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animation")
		bool bCanMoveRight;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animation")
		bool bMovingLeft;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animation")
		bool bMovingRight;
public:
	UMyAnimInstance();

	//Overriding the Native initialize and update animation functions
	virtual void NativeInitializeAnimation() override;
	virtual void NativeUpdateAnimation(float DeltaSeconds) override;

private:
	//cache the PawnOwner
	APawn* PawnOwner{ nullptr };

	/*
	*\Debugging function
	*\Note:: we can have this as a global helper function
	*/
	UFUNCTION()
		void DebugLog(const FString& msg, const FColor& color);
};
