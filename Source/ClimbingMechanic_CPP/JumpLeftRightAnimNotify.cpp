// Fill out your copyright notice in the Description page of Project Settings.

#include "JumpLeftRightAnimNotify.h"
#include "Runtime/Engine/Classes/Engine/Engine.h"
#include "Components/SkeletalMeshComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "MyAnimInstance.h"
#include "ClimbingMechanic_CPPCharacter.h"


void UJumpLeftRightAnimNotify::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	Super::Notify(MeshComp, Animation);

	DebugLog(__FUNCTION__, FColor::Cyan);

	if (MeshComp)
	{
		//set the character movement mode to stop immediately 
		AClimbingMechanic_CPPCharacter* PlayerCharacter = Cast<AClimbingMechanic_CPPCharacter>(MeshComp->GetOwner());
		UAnimMontage* JumpAnimMontage = nullptr;
		if (PlayerCharacter)
		{
			JumpAnimMontage = PlayerCharacter->GetActiveJumpAnimMontage();
		}
		//Do a MontageStop of the current active jump montage; 
		UAnimInstance* AnimInstance = MeshComp->GetAnimInstance();
		if (AnimInstance)
		{
			UMyAnimInstance* CustomPlayerAnimInstance = Cast<UMyAnimInstance>(AnimInstance);
			if (CustomPlayerAnimInstance)
			{
				if (JumpAnimMontage)
				{
					CustomPlayerAnimInstance->Montage_Stop(0.f, JumpAnimMontage);
				}
			}
		}

		if (PlayerCharacter)
		{
			PlayerCharacter->SetSidewayJumping(false);
			PlayerCharacter->GetCharacterMovement()->StopMovementImmediately();
			//send a notification to the PlayerCharacter to perform the ledge grab
			PlayerCharacter->OnJumpLeftRightComplete();
		}
	}
}

void UJumpLeftRightAnimNotify::DebugLog(const FString& msg, const FColor& color)
{
	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(-1, 1, color, msg);
	}
}
