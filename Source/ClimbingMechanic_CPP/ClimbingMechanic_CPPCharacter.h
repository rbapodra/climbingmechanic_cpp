// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "ClimbingMechanic_CPPCharacter.generated.h"

//forward declaration
class UAnimMontage;
class UArrowComponent;

UCLASS(config=Game)
class AClimbingMechanic_CPPCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;

	/* Simple arrow components to be used as a guide to detect the right and left edge of the capsule going out of bounds while moving sideways on the ledge*/
	UPROPERTY(VisibleAnywhere, Category = "LedgeTrace")
		class UArrowComponent* LeftArrow;

	UPROPERTY(VisibleAnywhere, Category = "LedgeTrace")
		class UArrowComponent* RightArrow;

	UPROPERTY(VisibleAnywhere, Category = "LedgeTrace")
		class UArrowComponent* UpArrow;

	UPROPERTY(VisibleAnywhere, Category = "LedgeTrace")
		class UArrowComponent* LeftEdgeArrow;

	UPROPERTY(VisibleAnywhere, Category = "LedgeTrace")
		class UArrowComponent* RightEdgeArrow;

	/*
	*\Debugging function
	*/
	UFUNCTION()
		void DebugLog(const FString& msg, const FColor& color);

public:
	AClimbingMechanic_CPPCharacter();

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;
protected:

	/** Resets HMD orientation in VR. */
	void OnResetVR();

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/** 
	 * Called via input to turn at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/*
	* Called via mouse input to turn the controller
	* Its just a custom function to encapsulate the default turn behavior
	*\@param value - input mouse rotation value
	*/
	void Turn(float Value);

	/**
	 * Called via input to turn look up/down at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//Overriding the Jump function to handle the hanging and climbing events
	virtual void Jump() override;
	
protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

private:
	bool bCanTrace{ false };
	bool bClimbingLedge{ false };
	bool bIsHanging{ false };
	//These are movement booleans to move sideways when hanging from a ledge and not for a regular character sideway movement
	bool bCanMoveLeft{ false };
	bool bCanMoveRight{ false };
	bool bMovingLeft{ false };
	bool bMovingRight{ false };
	bool bCanTurnLeft{ false };
	bool bCanTurnRight{ false };
	bool bCanJumpLeft{ false };
	bool bCanJumpRight{ false };
	bool bCanJumpUp{ false };
	bool bIsSideWayJumping{ false }; 
	bool bIsJumpingUp{ false }; /* This boolean represents if a character is jumping up from a ledge and not for a regular jump */

	UFUNCTION()
		bool DoSphereTrace(const FVector& Start, const FVector& End, float radius, FHitResult& OutHit);

	UFUNCTION()
		bool DoLineTrace(const FVector& Start, const FVector& End, FHitResult& OutHit);

	UFUNCTION()
		bool DoCapsuleTrace(const FVector& Start, const FVector& End, FHitResult& OutHit);

	UFUNCTION()
		bool CanDoForwardLedgeTrace();

	UFUNCTION()
		bool CanDoHeightLedgeTrace();

	UFUNCTION()
		bool CanMoveLeftRightLedgeTrace(const class UArrowComponent* DirectionArrowComponent);

	UFUNCTION()
		bool CanJumpLeftRightLedgeTrace(const class UArrowComponent* DirectionArrowComponent);

	UFUNCTION()
		bool CanJumpUpLedgeTrace();

	UFUNCTION()
		bool CanTurnLeftRightLedgeTrace(float Value);

	/*
	*\Check if the character's hip or pelvis is within the ledge height range so the character can climb 
	*/
	UFUNCTION()
		bool IsPelvisInRange();

	UFUNCTION()
		bool CanGrabLedge();
	
	/*
	*\Perform the Ledge grab operation; here the character will move to a hanging state
	*/
	UFUNCTION()
		void PerformLedgeGrab();

	/** Resetting the hang state and jumping down back into the idle state */
	UFUNCTION()
		void ExitLedge();

	/*
	*\ Perform the climbing action 
	*/
	UFUNCTION()
		void ClimbLedge();

	/*
	*\Move sideways while Hanging
	*\param[in]: inputDirection: float value of the input axis that will cause the player to move left/right 
	*\param[in]: Direction: direction of the right vector
	*/
	UFUNCTION()
		void MoveSidewaysWhileHanging(float inputDirection, const FVector& Direction);

	/*
	*\Jump sideways while hanging when the player reaches the edge of a ledge
	*\param[in]: jumpDir: float value of the input axis that will cause the player to move left/right
	*\
	*/
	UFUNCTION()
		void JumpSidewaysWhileHanging(float jumpDir);

	/*
	*\Perform an upward jump when hanging from a ledge and then grab a ledge on top of the current one
	*/
	UFUNCTION()
		void JumpUp();

	/*
	*\Turn left / right when the player reaches the edge of a ledge and he is not performing a jump
	*\param[in]: turnDir: float value of the input axis representing the direction of motion
	*/
	UFUNCTION()
		void TurnLeftRightWhileHanging(float turnDir);
	
	FVector LedgeImpactLocation;
	FVector LedgeImpactNormal;
	FVector LedgeHeightLocation;

	//Note:: if we want we can make this blueprint accessible and assign the climb montage from there.
	class UAnimMontage* ClimbAnimMontage{ nullptr };
	class UAnimMontage* JumpLeftMontage{ nullptr };
	class UAnimMontage* JumpRightMontage{ nullptr };
	class UAnimMontage* JumpUpMontage{ nullptr };

	class UAnimMontage* ActiveJumpMontage{ nullptr }; //to keep track of which jump animation is playing, so we can stop it when we receive the End notification

	class UAnimMontage* TurnLeftMontage{ nullptr };
	class UAnimMontage* TurnRightMontage{ nullptr };
	class UAnimMontage* ActiveTurnMontage{ nullptr };

	UFUNCTION(BlueprintCallable)
		void InterpComplete(); //this is a call-back function to be called at the end of the move operation. We need to make it BlueprintCallable for it to be called from blue-prints since our character is a BP object derived from this class

	/*
	*\This is a call-back to be called at the end of a delay once the turn animation completes and the player has grabbed the ledge.
	*/
   UFUNCTION(BlueprintCallable)
	void OnTurnLeftRightCompleteTimeElapsed();

public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }	
	//Getter for the Climb montage
	FORCEINLINE class UAnimMontage* GetClimbMontage() const { return ClimbAnimMontage; }
	//Getter for the Jump Left/Right montages
	FORCEINLINE class UAnimMontage* GetJumpLeftMontage() const { return JumpLeftMontage; }
	FORCEINLINE class UAnimMontage* GetJumpRightMontage() const { return JumpRightMontage; }
	FORCEINLINE class UAnimMontage* GetActiveJumpAnimMontage() const { return ActiveJumpMontage; }
	//Getter for the Turn Left/Right montages
	FORCEINLINE class UAnimMontage* GetTurnLeftAnimMontage() const { return TurnLeftMontage; }
	FORCEINLINE class UAnimMontage* GetTurnRightAnimMontage() const { return TurnRightMontage; }
	FORCEINLINE class UAnimMontage* GetActiveTurnAnimMontage() const { return ActiveTurnMontage; }

	//Public setter for bClimbingLedge
	FORCEINLINE void SetClimbingLedge(bool bVal) { bClimbingLedge = bVal; }	
	FORCEINLINE void SetSidewayJumping(bool bVal) { bIsSideWayJumping = bVal; }
	FORCEINLINE void SetJumpingUp(bool bVal) { bIsJumpingUp = bVal; }

	/* Returns the arrow components */
	FORCEINLINE class UArrowComponent* GetLeftArrowComponent() const { return LeftArrow; }
	FORCEINLINE class UArrowComponent* GetRightArrowComponent() const { return RightArrow; }
	FORCEINLINE class UArrowComponent* GetLeftEdgeArrowComponent() const { return LeftEdgeArrow; }
	FORCEINLINE class UArrowComponent* GetRightEdgeArrowComponent() const { return RightEdgeArrow; }

	/*
	*\This function is a callback function that will be called from the JumpLeftRightAnimNotify function when the animation ends so that we can start the ClimbLedge function
	*/
	void OnJumpLeftRightComplete();

	/*
	*\Call back called at the end of the Turn animation
	*/
	void OnTurnLeftRightAnimComplete();

	/*
	*\Callback called at the end of the JumpUp animation
	*/
	void OnJumpUpComplete();
};

