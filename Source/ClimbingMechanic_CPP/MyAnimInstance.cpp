// Fill out your copyright notice in the Description page of Project Settings.

#include "MyAnimInstance.h"
#include "ClimbingMechanic_CPPCharacter.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Runtime/Engine/Classes/Engine/Engine.h"

UMyAnimInstance::UMyAnimInstance()	
{
	bIsInAir = false;
	Speed = 0.f;
	bIsHanging = false;
}

void UMyAnimInstance::NativeInitializeAnimation()
{
	Super::NativeInitializeAnimation();

	//cache the pawn owner here
	//because this is sort of a post initialize function, we can safely call this function here rather than in the constructor when all the information may not be available just yet
	PawnOwner = TryGetPawnOwner();
}

void UMyAnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	Super::NativeUpdateAnimation(DeltaSeconds);
	
	//make sure the Owner is valid
	if (!PawnOwner)
	{
		return; 
	}

	//Now make sure the owner is of the type of our character and not any other
	if (PawnOwner->IsA(AClimbingMechanic_CPPCharacter::StaticClass()))
	{
		//cast the PawnOwner to our character
		AClimbingMechanic_CPPCharacter* PlayerCharacter = Cast<AClimbingMechanic_CPPCharacter>(PawnOwner);
		if (PlayerCharacter)
		{
			bIsInAir = PlayerCharacter->GetMovementComponent()->IsFalling();
			Speed = PlayerCharacter->GetVelocity().Size(); //or use UKismet	

			//Print these
			//FString InAirMsg("IsInAir: ");
			//InAirMsg += bIsInAir ? "True" : "False";
			//DebugLog(InAirMsg, FColor::Red);

			//FString IsHanging("IsHanging: ");
			//IsHanging += bIsHanging ? "True" : "False";
			//DebugLog(IsHanging, FColor::Red);
		}
	}
}

/*
*\Print a debug message on the screen
*/
void UMyAnimInstance::DebugLog(const FString& msg, const FColor& color)
{
	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(-1, 1, color, msg);
	}
}