// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ClimbingMechanic_CPPGameMode.generated.h"

UCLASS(minimalapi)
class AClimbingMechanic_CPPGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AClimbingMechanic_CPPGameMode();
};



