// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNotifies/AnimNotify.h"
#include "JumpLeftRightAnimNotify.generated.h"

/**
 * 
 */
UCLASS()
class CLIMBINGMECHANIC_CPP_API UJumpLeftRightAnimNotify : public UAnimNotify
{
	GENERATED_BODY()
	
	virtual void Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation) override;

	/*
	*\Debugging function
	*\Note:: we can have this as a global helper function
	*/
	UFUNCTION()
		void DebugLog(const FString& msg, const FColor& color);	
};
