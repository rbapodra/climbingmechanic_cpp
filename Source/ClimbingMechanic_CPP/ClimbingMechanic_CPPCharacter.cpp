// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "ClimbingMechanic_CPPCharacter.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "Components/SphereComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/SceneComponent.h"
#include "Components/ArrowComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/PlayerController.h"
#include "Runtime/Engine/Classes/Engine/Engine.h"
#include "DrawDebugHelpers.h"
#include "Runtime/Engine/Classes/Kismet/KismetSystemLibrary.h"
#include "Runtime/Engine/Public/TimerManager.h"
#include "UObject/ConstructorHelpers.h"
#include "MyAnimInstance.h"

//////////////////////////////////////////////////////////////////////////
// AClimbingMechanic_CPPCharacter

AClimbingMechanic_CPPCharacter::AClimbingMechanic_CPPCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)

	//initializing the climb montage here 
	static ConstructorHelpers::FObjectFinder<UAnimMontage> ClimbAnimMontageObj(TEXT("/Game/Mannequin/Animations/Climb_Montage"));
	if (ClimbAnimMontageObj.Succeeded())
	{
		ClimbAnimMontage = ClimbAnimMontageObj.Object;
	}

	//Create the arrow components and attach them to the root object
	LeftArrow = CreateDefaultSubobject<UArrowComponent>(TEXT("LeftArrow"));
	LeftArrow->SetupAttachment(RootComponent);
	LeftArrow->SetRelativeLocation(FVector(40.f, -60.f, 40.f));

	RightArrow = CreateDefaultSubobject<UArrowComponent>(TEXT("RightArrow"));
	RightArrow->SetupAttachment(RootComponent);
	RightArrow->SetRelativeLocation(FVector(40.f, 60.f, 40.f));
		
	UpArrow = CreateDefaultSubobject<UArrowComponent>(TEXT("UpArrow"));
	UpArrow->SetupAttachment(RootComponent);
	UpArrow->SetRelativeLocation(FVector(65.f, 0.f, 290.f));

	LeftEdgeArrow = CreateDefaultSubobject<UArrowComponent>(TEXT("LeftEdgeArrow"));
	LeftEdgeArrow->SetupAttachment(RootComponent);
	LeftEdgeArrow->SetRelativeLocation(FVector(50.f, -150.f, 40.f));

	RightEdgeArrow = CreateDefaultSubobject<UArrowComponent>(TEXT("RightEdgeArrow"));
	RightEdgeArrow->SetupAttachment(RootComponent);
	RightEdgeArrow->SetRelativeLocation(FVector(50.f, 150.f, 40.f));

	//initialize the left and right jump montage here
	static ConstructorHelpers::FObjectFinder<UAnimMontage> JumpLeftAnimMontageObj(TEXT("/Game/Mannequin/Animations/JumpLeft_Montage"));
	if (JumpLeftAnimMontageObj.Succeeded())
	{
		JumpLeftMontage = JumpLeftAnimMontageObj.Object;
	}

	static ConstructorHelpers::FObjectFinder<UAnimMontage> JumpRightAnimMontageObj(TEXT("/Game/Mannequin/Animations/JumpRight_Montage"));
	if (JumpRightAnimMontageObj.Succeeded())
	{
		JumpRightMontage = JumpRightAnimMontageObj.Object;
	}

	//initialize the left and right turn montages here
	static ConstructorHelpers::FObjectFinder<UAnimMontage> TurnLeftAnimMontageObj(TEXT("/Game/Mannequin/Animations/CornerLeft_Montage"));
	if (TurnLeftAnimMontageObj.Succeeded())
	{
		TurnLeftMontage = TurnLeftAnimMontageObj.Object;
	}

	static ConstructorHelpers::FObjectFinder<UAnimMontage> TurnRightAnimMontageObj(TEXT("/Game/Mannequin/Animations/CornerRight_Montage"));
	if (TurnRightAnimMontageObj.Succeeded())
	{
		TurnRightMontage = TurnRightAnimMontageObj.Object;
	}

	//initialize the jump up montage
	static ConstructorHelpers::FObjectFinder<UAnimMontage> JumpUpAnimMontageObj(TEXT("/Game/Mannequin/Animations/JumpUp_Montage"));
	if (JumpUpAnimMontageObj.Succeeded())
	{
		JumpUpMontage = JumpUpAnimMontageObj.Object;
	}
}

//////////////////////////////////////////////////////////////////////////
// Input

void AClimbingMechanic_CPPCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &AClimbingMechanic_CPPCharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAxis("MoveForward", this, &AClimbingMechanic_CPPCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AClimbingMechanic_CPPCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &AClimbingMechanic_CPPCharacter::Turn);
	PlayerInputComponent->BindAxis("TurnRate", this, &AClimbingMechanic_CPPCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AClimbingMechanic_CPPCharacter::LookUpAtRate);
	

	// handle touch devices
	PlayerInputComponent->BindTouch(IE_Pressed, this, &AClimbingMechanic_CPPCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &AClimbingMechanic_CPPCharacter::TouchStopped);

	// VR headset functionality
	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &AClimbingMechanic_CPPCharacter::OnResetVR);		
}


void AClimbingMechanic_CPPCharacter::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void AClimbingMechanic_CPPCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
		Jump();
}

void AClimbingMechanic_CPPCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
		StopJumping();
}

void AClimbingMechanic_CPPCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AClimbingMechanic_CPPCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AClimbingMechanic_CPPCharacter::Turn(float Value)
{
	//if (!bIsHanging) //this would lock the side-ways rotation when the character is hanging but still allow look up  -> one way of preventing the side way movement failing the raytrace because of the change in the rotation and hence locking up
	{
		APawn::AddControllerYawInput(Value);
	}
}

void AClimbingMechanic_CPPCharacter::MoveForward(float Value)
{
	//if we are hanging, then based on the "Value", we will either ignore or move down	
	if ((Controller != NULL) && (Value != 0.0f))
	{
		if (!bIsHanging)
		{
			// find out which way is forward
			const FRotator Rotation = Controller->GetControlRotation();
			const FRotator YawRotation(0, Rotation.Yaw, 0);

			// get forward vector
			const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
			AddMovementInput(Direction, Value);
		}		
		else
		{
			if (Value == -1.0f)
			{
				ExitLedge();
			}
		}
	}
}

void AClimbingMechanic_CPPCharacter::MoveRight(float Value)
{	
	if ( (Controller != NULL) && (Value != 0.0f))
	{		
		//If we are not hanging, perform normal movement to the right / left depending on the direction 
		if (!bIsHanging)
		{
			//Use the controller direction here 
			// find out which way is right
			const FRotator Rotation = Controller->GetControlRotation();
			const FRotator YawRotation(0, Rotation.Yaw, 0);
			// get right vector 
			const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

			// add movement in that direction
			AddMovementInput(Direction, Value);
			//also reset the moving variables 
			bMovingLeft = false;
			bMovingRight = false;
			bIsSideWayJumping = false;
			bCanTurnLeft = false;
			bCanTurnRight = false;			
		}
		else
		{	
			//Before processing the Jump and Turn logic, check if the Jump key was pressed simultaneously
			//This needs to be done so that the player does NOT do a turn left/right instead of a side jump because the direction input overrides the Jump input which then gets ignored
			//Hence we will NOT process the below logic if a direction key is pressed along with the Jump key since it is not required
			//Early detection
			APlayerController* playerController = Cast<APlayerController>(Controller);
			if (playerController->IsInputKeyDown(EKeys::SpaceBar) && (bCanJumpLeft || bCanJumpRight || bIsSideWayJumping))
			{
				return;
			}
			
			//While hanging, if we want to move sideways, we have to use the ActorRotation instead of the Controller rotation in-order to prevent in correct sideway movement
			const FRotator Rotation = GetActorRotation();
			const FRotator YawRotation(0, Rotation.Yaw, 0);
			const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

			//reset the canMove Left/Right booleans to true initially and then based on the input, we can update their state						
			//check if we can move side-ways on the ledge based on the direction						
			if (Value > 0.f)
			{
				bCanMoveRight = CanMoveLeftRightLedgeTrace(GetRightArrowComponent());
				//update the anim instance boolean 
				UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
				if (AnimInstance)
				{
					UMyAnimInstance* CustomPlayerAnimInstance = Cast<UMyAnimInstance>(AnimInstance);
					if (CustomPlayerAnimInstance)
					{
						CustomPlayerAnimInstance->bCanMoveRight = bCanMoveRight;
					}
				}

				if (bCanMoveRight)
				{					
					MoveSidewaysWhileHanging(Value, Direction);
					//reset the bCanJumpRight boolean because we can't jump if we can move right
					bCanJumpRight = false;					
				}
				else
				{
					//check if we are at the edge and can Jump					
					bCanJumpRight = CanJumpLeftRightLedgeTrace(GetRightEdgeArrowComponent());
					//TODO:: if we can jump, also check if we can MoveRight so that we can reset the can jump boolean
					//this is because we might jump across a ledge on which we cannot move	

					if (bCanJumpRight)
					{
						bCanTurnRight = false;
					}
					else
					{
						bCanTurnRight = CanTurnLeftRightLedgeTrace(Value);
						bCanTurnRight = false;
						if (bCanTurnRight)
						{
							//Set the active turn montage
							ActiveTurnMontage = TurnRightMontage;
							TurnLeftRightWhileHanging(Value);
						}						
					}
				}
			}
			else // < 0.f because we already checked for 0.f in the beginning
			{
				bCanMoveLeft = CanMoveLeftRightLedgeTrace(GetLeftArrowComponent());
				//update the anim instance boolean 
				UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
				if (AnimInstance)
				{
					UMyAnimInstance* CustomPlayerAnimInstance = Cast<UMyAnimInstance>(AnimInstance);
					if (CustomPlayerAnimInstance)
					{
						CustomPlayerAnimInstance->bCanMoveLeft = bCanMoveLeft;
					}
				}

				if (bCanMoveLeft)
				{					
					MoveSidewaysWhileHanging(Value, Direction);
					//reset the bCanJumpLeft boolean because we can't jump if we can move left
					bCanJumpLeft = false;					
				}
				else
				{
					//first check if we are at the edge and can Jump
					bCanJumpLeft = CanJumpLeftRightLedgeTrace(GetLeftEdgeArrowComponent());
					//TODO:: if we can jump, also check if we can MoveLeft so that we can reset the can jump boolean
					//this is because we might jump across a ledge on which we cannot move		
					
					//if we can't jump, then check if we can turn left; we will any way be unable to move left since we are at the edge
					if (bCanJumpLeft)
					{
						bCanTurnLeft = false;
						/*FString message("Can Jump Left : ");
						message += "True";
						DebugLog(message, FColor::Green);
						*/
					}
					else
					{
						bCanTurnLeft = CanTurnLeftRightLedgeTrace(Value);
						/*FString message("Can Jump Left : ");
						message += "False";
						DebugLog(message, FColor::Red);*/
						
						if (bCanTurnLeft)
						{
							//Set the active turn montage
							ActiveTurnMontage = TurnLeftMontage;
							TurnLeftRightWhileHanging(Value);
						}
						
					}
				}				
			}
						
		}
	}
	else if (Controller != NULL && Value == 0.0f) //this is when we release the right axis or button
	{
		if (bIsHanging)
		{
			//reset the moving left/right booleans to stop the animation
			bMovingLeft = false;
			bMovingRight = false;
			//update the anim instance boolean 
			UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
			if (AnimInstance)
			{
				UMyAnimInstance* CustomPlayerAnimInstance = Cast<UMyAnimInstance>(AnimInstance);
				if (CustomPlayerAnimInstance)
				{
					CustomPlayerAnimInstance->bMovingLeft = bMovingLeft;
					CustomPlayerAnimInstance->bMovingRight = bMovingRight;
				}
			}
		}
	}
}

void AClimbingMechanic_CPPCharacter::Jump()
{
	if (!bIsHanging)
	{
		Super::Jump();
	}
	else
	{
		//Here we need to check if we can jump side-ways, if not: then it must be to climb ledge
		//here the player must have pressed Direction + Jump key in order to jump sideways across the ledge			
		//check the axis value which will determine if the player has pressed left/right key
		//Note:: if we don't add the condition of Value != 0.f then the player will not Climb the ledge if NO direction button is pressed
		float Value = GetInputAxisValue("MoveRight");
		if ((bCanJumpLeft || bCanJumpRight) && Value != 0.f) 
		{
			DebugLog("Can Jump Sideways!", FColor::Orange);			
			JumpSidewaysWhileHanging(Value);
		}
		else
		{
			//reset the bCanJumpLeft and Right booleans 
			bCanJumpLeft = false;
			bCanJumpRight = false;

			//First check if the player can climb up
			bCanJumpUp = CanJumpUpLedgeTrace();
			if (!bCanJumpUp)
			{
				bIsJumpingUp = false;

				//Climb the ledge
				ClimbLedge();
			}			
			else
			{				
				DebugLog("Can Jump Up!", FColor::Green);
				JumpUp();
			}
		}
		
	}
}

/*
*\Plays the Jump Up Montage and once done it grabs the ledge
*/
void AClimbingMechanic_CPPCharacter::JumpUp()
{
	if (!bIsJumpingUp)
	{
		//set the character's movement mode to flying
		GetCharacterMovement()->SetMovementMode(MOVE_Flying);

		//Play the Jump montage; 
		UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
		if (AnimInstance)
		{
			UMyAnimInstance* CustomPlayerAnimInstance = Cast<UMyAnimInstance>(AnimInstance);
			if (CustomPlayerAnimInstance)
			{							
				if (JumpUpMontage)
				{
					CustomPlayerAnimInstance->Montage_Play(JumpUpMontage, 1.f, EMontagePlayReturnType::MontageLength, 0.f, true);
					ActiveJumpMontage = JumpUpMontage;
				}
			}
		}

		bIsJumpingUp = true;
		bIsHanging = true;

		//Disable the input as well
		APlayerController* playerController = Cast<APlayerController>(Controller);
		DisableInput(playerController);		
	}
}

/*
*\Do a forward ledge trace to detect the ledge
*/
bool AClimbingMechanic_CPPCharacter::CanDoForwardLedgeTrace()
{
	//we will do a sphere trace to detect the ledge
	FVector Start = GetActorLocation(); //start at the character

	FVector Forward = GetActorRotation().Vector();	//this will give the forward vector after rotation.
	
	//End vector will be based on the standard ray equation. we will multiply the forward vector by a scalar and add it to the Start
	FVector End = Start + Forward * 150.f;
	
	//Draw the debug capsule here instead of inside the DoSphereTrace function since some of the values change for the 2 trace methods, namely the length and the rotation
	FCollisionShape SphereTrace = FCollisionShape::MakeSphere(20.f);	
	FQuat TraceRotation = GetActorRotation().Quaternion() * FRotator(90.0f, 0.f, 0.f).Quaternion();
	FVector CapsuleCenter = Start + (End - Start) * 0.5f;
	DrawDebugCapsule(GetWorld(), CapsuleCenter, 96, SphereTrace.GetSphereRadius(), TraceRotation, FColor::Red);

	FHitResult OutHit;
	bool bHit = DoSphereTrace(Start, End, 20.f, OutHit);
	//bool bHit = DoLineTrace(Start, End, OutHit);
	//if bHit = true, save the impact location and normal for the vertical trace
	if (bHit)
	{
		//DebugLog(L"ForwardTrace passed", FColor::Green);
		float HitCapsuleHalfHeight = (End - OutHit.ImpactPoint).Size() * 0.5f;
		DrawDebugCapsule(GetWorld(), OutHit.Location, HitCapsuleHalfHeight, SphereTrace.GetSphereRadius(), TraceRotation, FColor::Green);
		DrawDebugPoint(GetWorld(), OutHit.ImpactPoint, 16.f, FColor::Blue);

		//Note: at initial point of contact ImpactPoint and ImpactNormal are the same as Location and Normal
		LedgeImpactLocation = OutHit.Location;
		LedgeImpactNormal = OutHit.Normal;
	}

	return bHit;
}

/*
*\Do a height trace to determine the height of the ledge by finding the impact point on the top
*\Note:: passing this test does not mean the ledge can be climbed from the current player's position. The ledge could be taller.
*/
bool AClimbingMechanic_CPPCharacter::CanDoHeightLedgeTrace()
{
	FVector ForwardVector = GetActorRotation().Vector();
	FVector ForwardOffset = ForwardVector * 70.f;
	//start the trace at an offset above the character
	FVector Start = GetActorLocation();
	Start.Z += 500.f; //+ve offset to move up	
	Start += ForwardOffset;

	FVector End = Start;
	End.Z -= 500.f; // coming down by 500 units

	//Draw a debug capsule
	FCollisionShape SphereTrace = FCollisionShape::MakeSphere(20.f);
	FQuat TraceRotation = GetActorRotation().Quaternion();
	FVector CapsuleCenter = Start + (End - Start) * 0.5f;
	DrawDebugCapsule(GetWorld(), CapsuleCenter, 270, SphereTrace.GetSphereRadius(), TraceRotation, FColor::Red);
	//270 = 250 + 20 //Half of moving down + sphere radius

	FHitResult OutHit;
	bool bHit = DoSphereTrace(Start, End, 20.f, OutHit);
	//bool bHit = DoLineTrace(Start, End, OutHit);
	if (bHit)
	{		
		//DebugLog(L"Height Trace passed!", FColor::Green);
		float HitCapsuleHalfHeight = (End - OutHit.ImpactPoint).Size() * 0.5f;
		FVector HitCapsuleCenter = CapsuleCenter + (OutHit.ImpactPoint - CapsuleCenter) * 0.5f;
		HitCapsuleCenter.Z = HitCapsuleCenter.Z - (HitCapsuleHalfHeight * 2.f); //trying to position the top part of the capsule at the impact point
		DrawDebugCapsule(GetWorld(), HitCapsuleCenter, HitCapsuleHalfHeight, SphereTrace.GetSphereRadius(), TraceRotation, FColor::Green);
		
		DrawDebugPoint(GetWorld(), OutHit.ImpactPoint, 16.f, FColor::Blue);

		LedgeHeightLocation = OutHit.Location;
	}

	return bHit;
}

/*
*\ Do a capsule trace to detect if the player can move left / right while hanging on the ledge
*\Note: It is assumed that the player is in the hang state; bHanging boolean status will be checked when the input is received
*\param[in]: Left/Right arrow component based on the direction detected during input
*/
bool AClimbingMechanic_CPPCharacter::CanMoveLeftRightLedgeTrace(const UArrowComponent* DirectionArrowComponent)
{
	if (!DirectionArrowComponent)
	{
		return false;
	}

	//we are trying to test if we can put another capsule which is within bounds of the ledge
	/*float Direction = GetInputAxisValue("MoveRight");
	FVector Start = GetActorLocation() + (Direction * GetCapsuleComponent()->GetUnscaledCapsuleRadius()/4); 	
	FVector Forward = GetActorRotation().Vector();	//this will give the forward vector after rotation.
	FVector End = Start + Forward * 150.f;
	*/

	//Trying with the arrow components
	FVector Start = DirectionArrowComponent->GetComponentLocation(); //in world space		
	FVector End = Start;
	
	//Draw a debug capsule
	DrawDebugCapsule(GetWorld(), Start, 60.f, 20.f, FQuat::Identity, FColor::Green);

	FHitResult OutHit;
	bool bHit = DoCapsuleTrace(Start, End, OutHit);
	if (!bHit) //this means we can't move in the pressed direction
	{
		//Draw the capsule in red
		DrawDebugCapsule(GetWorld(), Start, 60.f, 20.f, FQuat::Identity, FColor::Red);
	}
	return bHit;
}

/*
*\Do a capsule trace to determine if the player can Jump left/right
*/
bool AClimbingMechanic_CPPCharacter::CanJumpLeftRightLedgeTrace(const class UArrowComponent* DirectionArrowComponent)
{
	if (!DirectionArrowComponent)
	{
		return false;
	}
	
	if (bIsSideWayJumping)
	{
		//Note: Should we return true? Yes because if we return false, the code will process the TurnLeftRight trace and we don't want the player to do a turn left
		return true;
	}

	FVector Start = DirectionArrowComponent->GetComponentLocation();
	FVector End = Start;

	//Draw a debug capsule
	DrawDebugCapsule(GetWorld(), Start, 60.f, 20.f, FQuat::Identity, FColor::Green);

	FHitResult OutHit;
	bool bHit = DoCapsuleTrace(Start, End, OutHit);
	if (!bHit) //this means we can't move in the pressed direction
	{
		//Draw the capsule in red
		DrawDebugCapsule(GetWorld(), Start, 60.f, 20.f, FQuat::Identity, FColor::Red);
	}
	return bHit;
}

/*
*\ When the player is hanging and if he wants to grab another ledge above  by jumping, to do this we do a ray trace of the Up Arrow component with any objects above
*\ If true, the player jumps and grabs the ledge
*\ If false, player doesn't do anything; 
*\ Note: Another possible solution could be we always let the player jump. if we cannot grab the ledge he will simply fall down and try to grab the ledge
*/
bool AClimbingMechanic_CPPCharacter::CanJumpUpLedgeTrace()
{
	if (!UpArrow)
	{
		return false;
	}

	//if we are already jumping, return true	
	if (bIsJumpingUp)
	{
		return true;
	}

	FVector Start = UpArrow->GetComponentLocation();
	FVector End = Start;

	//Draw a debug capsule 
	DrawDebugCapsule(GetWorld(), Start, 60.f, 20.f, FQuat::Identity, FColor::Green);

	FHitResult OutHit;
	bool bHit = DoCapsuleTrace(Start, End, OutHit);
	if (!bHit) //this means we can't move in the pressed direction
	{
		//Draw the capsule in red
		DrawDebugCapsule(GetWorld(), Start, 60.f, 20.f, FQuat::Identity, FColor::Red);
	}
	return bHit;
}

/*
*\Do a ray-trace to determine if the player can turn left on the ledge
*\Use sphere trace with the location of the Arrow component
*\Param[in]: Value -> input axis scale pressed direction indicating left / right
*/
bool AClimbingMechanic_CPPCharacter::CanTurnLeftRightLedgeTrace(float Value)
{
	UArrowComponent* CurrentArrowComponent = (Value == 1.f) ? GetRightArrowComponent() : GetLeftArrowComponent();
	if (!CurrentArrowComponent)
	{
		return false;
	}

	FVector  Start = CurrentArrowComponent->GetComponentLocation();
	//offset the Z-Component to raise the arrow to the top of the ledge (going up by capsule half-height)
	Start.Z += 60.f;

	//In the final position, the left/right arrow will be at 90 degrees from the current position. So we need to take the right unit vector and use this as the direction of the new position of the arrow component	
	const FRotator arrowRotation = CurrentArrowComponent->GetComponentRotation();
	const FRotator YawRotation(0, arrowRotation.Yaw, 0);
	const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);

//	FVector Forward = GetLeftArrowComponent()->GetComponentRotation().Vector();
//	Forward *= 70.f;
	FQuat TraceRotation = arrowRotation.Quaternion() * FRotator(90.0f, 0.f, 0.f).Quaternion();
	FVector End = Start + Direction * (70.f);
	//TODO:: draw a debug capsule
	DrawDebugCapsule(GetWorld(), Start, 60.f, 20.f, TraceRotation, FColor::Red);
	DrawDebugDirectionalArrow(GetWorld(), Start, End, 10.f, FColor::Magenta);

	FHitResult OutHit;
	bool bHit = DoSphereTrace(Start, End, 30.f, OutHit);
	return bHit;
}

bool AClimbingMechanic_CPPCharacter::IsPelvisInRange()
{
	//get the character's pelvis location	
	FVector PelvisLocation = GetMesh()->GetSocketLocation("PelvisSocket");

	//compare the z values of PelvisLocation and LedgeHeightLocation
	float HeightDiff = PelvisLocation.Z - LedgeHeightLocation.Z;
	return (HeightDiff >= -50.f && HeightDiff <= 0);
}

bool AClimbingMechanic_CPPCharacter::CanGrabLedge()
{
	return (IsPelvisInRange() && !bClimbingLedge);
}

void AClimbingMechanic_CPPCharacter::PerformLedgeGrab()
{
	//set the character's movement mode to flying
	GetCharacterMovement()->SetMovementMode(MOVE_Flying);
	bIsHanging = true;
	//Controller->SetIgnoreLookInput(true); -> by doing this we prevent the player controller to rotate and hence fix the issue of the freeze in the side-way movement while hanging and also moving the mouse 	
	//Controller->GetRootComponent()->bAbsoluteRotation = false; //this prevents the root component rotation but doesn't fix the issue of the freeze

	//Update the character's animation state to Hanging
	UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
	if (AnimInstance)
	{
		//set the Hanging boolean to true here as well that will cause the state machine to update its state (if the player is in jumpState)
		//In BP, we did this using the CanGrab custom event
		UMyAnimInstance* CustomPlayerAnimInstance = Cast<UMyAnimInstance>(AnimInstance);
		if (CustomPlayerAnimInstance)
		{
			CustomPlayerAnimInstance->bIsHanging = true; 			
		}
	}

	//move the character using interpolation to a particular location
	FVector MoveDirection = LedgeImpactNormal * FVector(22.0, 22.0, 0.0);	
	FVector MoveToRelativeLocation = FVector::ZeroVector;
	MoveToRelativeLocation.X = MoveDirection.X + LedgeImpactLocation.X;
	MoveToRelativeLocation.Y = MoveDirection.Y + LedgeImpactLocation.Y;
	MoveToRelativeLocation.Z = LedgeHeightLocation.Z - 120.f; //Note:: can this be capsuleHalfHeight
	//MoveToRelativeLocation = GetActorLocation();
	MoveToRelativeLocation.Z = LedgeHeightLocation.Z - 120.f;
	//the direction vector is the ledgeImpactNormal and we want the forward vector to be in its opposite direction	
	FRotator MoveToRelativeRotation = LedgeImpactNormal.Rotation();
	MoveToRelativeRotation.Yaw -= 180.f; //need to face the character in the opposite direction of that of the normal	
	//MoveToRelativeRotation = FRotator::ZeroRotator;

	FLatentActionInfo LatentInfo;
	LatentInfo.CallbackTarget = this;
	LatentInfo.ExecutionFunction = FName("InterpComplete");
	LatentInfo.Linkage = 0;
	LatentInfo.UUID = 0;
	UKismetSystemLibrary::MoveComponentTo(GetCapsuleComponent(), MoveToRelativeLocation, MoveToRelativeRotation, false, false, 0.13, false, EMoveComponentAction::Move, LatentInfo);	
}

void AClimbingMechanic_CPPCharacter::ExitLedge()
{
	//if we are hanging, reset the boolean and move down back to the idle state; 
	//if not, then ignore
	if (bIsHanging)
	{
		//update the character movement state to Walking
		GetCharacterMovement()->SetMovementMode(MOVE_Walking);
		bIsHanging = false;

		UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
		if (AnimInstance)
		{
			UMyAnimInstance* CustomPlayerAnimInstance = Cast<UMyAnimInstance>(AnimInstance);
			if (CustomPlayerAnimInstance)
			{
				CustomPlayerAnimInstance->bIsHanging = false;
			}
		}
	}
}

void AClimbingMechanic_CPPCharacter::ClimbLedge()
{
	//If we are not climbing, move to the climb state and play the climbing animation
	//If not, ignore
	if (!bClimbingLedge)
	{
		bClimbingLedge = true;

		//set the character movement mode to flying
		GetCharacterMovement()->SetMovementMode(MOVE_Flying);
		
		//reset the hanging bool
		bIsHanging = false;

		UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
		if (AnimInstance)
		{
			UMyAnimInstance* CustomPlayerAnimInstance = Cast<UMyAnimInstance>(AnimInstance);
			if (CustomPlayerAnimInstance)
			{
				//Play the climbing animation and then once it is done, set the climbing boolean to false and also set the hanging bool to false
				//which in turn advances the player state machine
				
				if (ClimbAnimMontage)
				{
					CustomPlayerAnimInstance->Montage_Play(ClimbAnimMontage, 1.f, EMontagePlayReturnType::MontageLength, 0.f, true);
				}
				
				CustomPlayerAnimInstance->bIsHanging = false;
			}
		}		

		DebugLog("Climb Ledge!", FColor::Yellow);
	}
}

/*
*\Move the character left / right based on the direction by using interpolation 
*\The movement is left->right or vice versa only if the camera is behind. If the camera is perpendicular, then it becomes forward->backward
*\param[in]: inputDirection : axis value > 0 or < 0; usually either 1 or -1: 1 indicates right, -1 indicates left
*\param[in]: Direction: this is the actor right direction and not the controller right direction which is not applicable while hanging
*/
void AClimbingMechanic_CPPCharacter::MoveSidewaysWhileHanging(float inputDirection, const FVector& Direction)
{
	//if the y-value < 0, it means we are moving left otherwise right
	FVector Start = GetActorLocation();	
	//FVector End = Start + FVector(inputDirection, 0.f, 0.f) * 20.f; //This also works fine but only moving left->right: if we have to move forward-backward based on the placement of the ledge, we will need to use Direction
	FVector End = Start + Direction * (20.f * inputDirection);
	FVector FinalLocation = FMath::VInterpTo(Start, End, GetWorld()->GetDeltaSeconds(), 5.0);
	SetActorLocation(FinalLocation);	

	//based on the direction, update the bMoving booleans
	if (inputDirection > 0)
	{
		bMovingRight = true; //if this is true, the other is false
		bMovingLeft = false;
	}
	else //this will be < 0, the = 0 condition is already handled from before
	{
		bMovingLeft = true;
		bMovingRight = false;
	}
	//update the anim instance boolean 
	UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
	if (AnimInstance)
	{
		UMyAnimInstance* CustomPlayerAnimInstance = Cast<UMyAnimInstance>(AnimInstance);
		if (CustomPlayerAnimInstance)
		{
			CustomPlayerAnimInstance->bMovingLeft = bMovingLeft;
			CustomPlayerAnimInstance->bMovingRight = bMovingRight;
		}
	}
}

/*
*\Jump the character left / right based on the input and if the player is hanging on the edge of the ledge
*\After jumping, grab the ledge of the next object (which means there are 2 actions)
*/
void AClimbingMechanic_CPPCharacter::JumpSidewaysWhileHanging(float jumpDir)
{
	//if we are already jumping, ignore
	if (!bIsSideWayJumping && jumpDir != 0.f)
	{		
		//set the character's movement mode to flying
		GetCharacterMovement()->SetMovementMode(MOVE_Flying);

		//Play the Jump montage; 
		UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
		if (AnimInstance)
		{
			UMyAnimInstance* CustomPlayerAnimInstance = Cast<UMyAnimInstance>(AnimInstance);
			if (CustomPlayerAnimInstance)
			{
				//play the montage based on the direction
				ActiveJumpMontage = (jumpDir > 0.f) ? JumpRightMontage : JumpLeftMontage;								
				if (ActiveJumpMontage)
				{
					CustomPlayerAnimInstance->Montage_Play(ActiveJumpMontage, 1.f, EMontagePlayReturnType::MontageLength, 0.f, true);
				}				
			}
		}

		bIsSideWayJumping = true;
		bIsHanging = true;
	}
}

void AClimbingMechanic_CPPCharacter::TurnLeftRightWhileHanging(float turnDir)
{
	if (turnDir != 0)
	{		
		if (ActiveTurnMontage)
		{
			//Disable input first
			APlayerController* playerController = Cast<APlayerController>(Controller);
			DisableInput(playerController);

			//Play the anim montage
			UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
			if (AnimInstance)
			{
				UMyAnimInstance* CustomPlayerAnimInstance = Cast<UMyAnimInstance>(AnimInstance);
				if (CustomPlayerAnimInstance)
				{										
					CustomPlayerAnimInstance->Montage_Play(ActiveTurnMontage, 1.f, EMontagePlayReturnType::MontageLength, 0.f, true);
				}
			}			
		}
	}
}

void AClimbingMechanic_CPPCharacter::InterpComplete()
{
	DebugLog(L"Moved Character", FColor::Red);	
	//Stop the character movement
	GetCharacterMovement()->StopMovementImmediately();
}

/*
*\Callback received from the JumpLeftRightAnimNotify class to start the PerformLedgeGrab process
*\We could also have implemented this as a latent function which would be a callback after a delay
*/
void AClimbingMechanic_CPPCharacter::OnJumpLeftRightComplete()
{
	PerformLedgeGrab();
}

void AClimbingMechanic_CPPCharacter::OnJumpUpComplete()
{
	PerformLedgeGrab();
	//Enable the input
	APlayerController* playerController = Cast<APlayerController>(Controller);
	EnableInput(playerController);
}

void AClimbingMechanic_CPPCharacter::OnTurnLeftRightAnimComplete()
{
	//reset the turning booleans(regardless) so that we don't keep repeating the turn animation in case the player keeps pressing the key or the directional axis
	bCanTurnLeft = false;
	bCanTurnRight = false;

	//Once the player stops turning, Grab the ledge and then enable the input
	PerformLedgeGrab();	

	//Add a delay before enabling the input to make sure no input is received during the ledge grab as well to prevent further turning	
	//Method 1: Use Kismet's Delay and a latent action info
	/*FLatentActionInfo LatentInfo;
	LatentInfo.CallbackTarget = this;
	LatentInfo.ExecutionFunction = FName("OnTurnLeftRightCompleteTimeElapsed");
	LatentInfo.Linkage = 0;
	LatentInfo.UUID = 1;
	UKismetSystemLibrary::Delay(GetWorld(), 0.2f, LatentInfo);
	*/
	//Method 2:
	FTimerHandle delayTimerHandle;
	GetWorldTimerManager().SetTimer(delayTimerHandle, this, &AClimbingMechanic_CPPCharacter::OnTurnLeftRightCompleteTimeElapsed, 0.2f, false);
}

void AClimbingMechanic_CPPCharacter::OnTurnLeftRightCompleteTimeElapsed()
{
	//Enable the input once the timer has elapsed
	APlayerController* playerController = Cast<APlayerController>(Controller);
	EnableInput(playerController);	
}

//Sphere collision	
bool AClimbingMechanic_CPPCharacter::DoSphereTrace(const FVector& Start, const FVector& End, float radius, FHitResult& OutHit)
{
	FCollisionShape SphereTrace = FCollisionShape::MakeSphere(radius);			
	bool bHit = GetWorld()->SweepSingleByChannel(OutHit, Start, End, FQuat::Identity, ECC_GameTraceChannel2, SphereTrace); //We want the trace channel to be Ledge (is this game channel 1?)

	return bHit;
}

//Line Trace
/*
*\Returns true if there is a line hit from the raycast from the character to the object.
*\Also returns the Hit result
*/
bool AClimbingMechanic_CPPCharacter::DoLineTrace(const FVector& Start, const FVector& End, FHitResult& OutHit)
{
	//Draw the debug line for illustration
	//Note:: we could use a different line color upto the blocking hit
		
	bool bHit = GetWorld()->LineTraceSingleByChannel(OutHit, Start, End, ECC_GameTraceChannel2);	
	if (bHit && OutHit.bBlockingHit) //bBlockingHit is going to be true since we set the channel to block
	{				
		//DebugLog("Ledge detected!");
		DrawDebugLine(GetWorld(), Start, OutHit.ImpactPoint, FColor::Red, true);
		DrawDebugLine(GetWorld(), OutHit.ImpactPoint, End, FColor::Green, true);
		DrawDebugPoint(GetWorld(), OutHit.ImpactPoint, 16.f, FColor::Blue, true);
	}
	else
	{
		//Draw red line through otherwise
		DrawDebugLine(GetWorld(), Start, End, FColor::Red, true);
	}

	return bHit;
}

/*
*\Capsule trace to determine if the player can move left or right
*/
bool AClimbingMechanic_CPPCharacter::DoCapsuleTrace(const FVector& Start, const FVector& End, FHitResult& OutHit)
{
	const float CapsuleRadius = 20.f;
	const float CapsuleHalfHeight = 60.f;
	FCollisionShape CapsuleTrace = FCollisionShape::MakeCapsule(CapsuleRadius, CapsuleHalfHeight);
	bool bHit = GetWorld()->SweepSingleByChannel(OutHit, Start, End, FQuat::Identity, ECC_GameTraceChannel2, CapsuleTrace);
	return bHit;
}

/*
*\Overriding the Character Tick function
*/
void AClimbingMechanic_CPPCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	//Note:We don't have to be running both the traces all the time
	if(CanDoForwardLedgeTrace()) //first detect the ledge
	{
		//Now check if it is climbable based on the ledge height
		if (CanDoHeightLedgeTrace())
		{
			if (CanGrabLedge()) //Now check if the player can jump to grab the ledge
			{
				DebugLog(L"Can grab ledge!", FColor::Green);
				PerformLedgeGrab();
			}
		}
				
	}				
}

/*
*\Print a debug message on the screen
*/
void AClimbingMechanic_CPPCharacter::DebugLog(const FString& msg, const FColor& color)
{
	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(-1, 1, color, msg);
	}
}