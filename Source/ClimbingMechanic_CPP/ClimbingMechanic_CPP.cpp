// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "ClimbingMechanic_CPP.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, ClimbingMechanic_CPP, "ClimbingMechanic_CPP" );
 