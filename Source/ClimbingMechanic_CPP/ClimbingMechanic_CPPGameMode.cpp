// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "ClimbingMechanic_CPPGameMode.h"
#include "ClimbingMechanic_CPPCharacter.h"
#include "UObject/ConstructorHelpers.h"

AClimbingMechanic_CPPGameMode::AClimbingMechanic_CPPGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
