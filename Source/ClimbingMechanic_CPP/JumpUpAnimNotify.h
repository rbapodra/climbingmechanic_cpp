// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNotifies/AnimNotify.h"
#include "JumpUpAnimNotify.generated.h"

/**
 * Class to handle the notifications sent by the JumpUpAnim Montage
 *\Note: this needs to be handled in a slightly different way (maybe per behavior) rather than create sub-classes for all the jump animations
 */
UCLASS()
class CLIMBINGMECHANIC_CPP_API UJumpUpAnimNotify : public UAnimNotify
{
	GENERATED_BODY()
	
	virtual void Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation) override;

	/*
	*\Debugging function
	*\Note:: we can have this as a global helper function
	*/
	UFUNCTION()
		void DebugLog(const FString& msg, const FColor& color);	
};
