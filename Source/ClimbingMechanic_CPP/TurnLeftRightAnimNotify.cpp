// Fill out your copyright notice in the Description page of Project Settings.

#include "TurnLeftRightAnimNotify.h"
#include "Runtime/Engine/Classes/Engine/Engine.h"
#include "Components/SkeletalMeshComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "MyAnimInstance.h"
#include "ClimbingMechanic_CPPCharacter.h"


/*
*\Send a notification when the particular animation reaches a particular point. In this case the end 
*/
void UTurnLeftRightAnimNotify::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	Super::Notify(MeshComp, Animation);

	DebugLog(__FUNCTION__, FColor::Cyan);

	if (MeshComp)
	{
		//TODO:: do we set the character movement mode to stop immediately ?
		AClimbingMechanic_CPPCharacter* PlayerCharacter = Cast<AClimbingMechanic_CPPCharacter>(MeshComp->GetOwner());
		UAnimMontage* TurnAnimMontage = nullptr;
		if (PlayerCharacter)
		{
			TurnAnimMontage = PlayerCharacter->GetActiveTurnAnimMontage();
		}
		//Do a MontageStop of the current active jump montage; 
		UAnimInstance* AnimInstance = MeshComp->GetAnimInstance();
		if (AnimInstance)
		{
			UMyAnimInstance* CustomPlayerAnimInstance = Cast<UMyAnimInstance>(AnimInstance);
			if (CustomPlayerAnimInstance)
			{
				if (TurnAnimMontage)
				{
					CustomPlayerAnimInstance->Montage_Stop(0.f, TurnAnimMontage);
				}
			}
		}

		if (PlayerCharacter)
		{			
			PlayerCharacter->GetCharacterMovement()->StopMovementImmediately();
			
			//send a notification to the PlayerCharacter to perform the ledge grab
			PlayerCharacter->OnTurnLeftRightAnimComplete();
		}
	}
}

void UTurnLeftRightAnimNotify::DebugLog(const FString& msg, const FColor& color)
{
	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(-1, 1, color, msg);
	}
}
