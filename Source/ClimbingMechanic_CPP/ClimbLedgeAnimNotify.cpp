// Fill out your copyright notice in the Description page of Project Settings.

#include "ClimbLedgeAnimNotify.h"
#include "Runtime/Engine/Classes/Engine/Engine.h"
#include "Components/SkeletalMeshComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "MyAnimInstance.h"
#include "ClimbingMechanic_CPPCharacter.h"

void UClimbLedgeAnimNotify::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	Super::Notify(MeshComp, Animation);

	DebugLog(__FUNCTION__, FColor::Cyan);
		
	if (MeshComp)
	{
		//set the character movement mode to walking and set the climbing ledge boolean to false				
		AClimbingMechanic_CPPCharacter* PlayerCharacter = Cast<AClimbingMechanic_CPPCharacter>(MeshComp->GetOwner());		
		UAnimMontage* ClimbMontage = nullptr;
		if (PlayerCharacter)
		{
			ClimbMontage = PlayerCharacter->GetClimbMontage();
		}
		//Do a MontageStop of the current climb montage; Note: we could use the default argument of stopping all the active montages as well
		UAnimInstance* AnimInstance = MeshComp->GetAnimInstance();
		if (AnimInstance)
		{
			UMyAnimInstance* CustomPlayerAnimInstance = Cast<UMyAnimInstance>(AnimInstance);
			if (CustomPlayerAnimInstance)
			{				
				if (ClimbMontage)
				{
					CustomPlayerAnimInstance->Montage_Stop(0.f, ClimbMontage);
				}				
			}
		}		
		
		if (PlayerCharacter)
		{
			PlayerCharacter->SetClimbingLedge(false);
			PlayerCharacter->GetCharacterMovement()->SetMovementMode(MOVE_Walking);
		}
	}
}

void UClimbLedgeAnimNotify::DebugLog(const FString& msg, const FColor& color)
{
	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(-1, 1, color, msg);
	}
}


